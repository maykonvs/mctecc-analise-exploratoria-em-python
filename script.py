# Seção 3.2.1 - The Adult Example

#%%
file = open('data/adult.data', 'r')

def chr_int(a):
    '''
    Funcao que converte uma string em inteiro, retornando 0 se isto
    nao for possivel.
    '''
    if a .isdigit():
        return int(a)
    else:
        return 0

data = []

# Importando os dados do arquivo e jogando para um vetor de vetores.
for line in file:
    data1 = line.split(', ')
    if len(data1) == 15:
        data.append([
            chr_int(data1[0]),
            data1[1],
            chr_int(data1[2]),
            data1[3] ,
            chr_int(data1[4]),
            data1[5],
            data1[6],
            data1[7],
            data1[8],
            data1[9],
            chr_int(data1[10]),
            chr_int(data1[11]),
            chr_int(data1[12]),
            data1[13],
            data1[14]
        ])

print(data[1:2])

#%%
import pandas as pd

# Jogando os dados para um DataFrame do pandas, que é bem mais estruturado
# para fazer consultas.
df = pd.DataFrame(data)
df.columns = [
    'age',
    'type_employer',
    'fnlwgt',
    'education',
    'education_num',
    'marital',
    'occupation',
    'relationship',
    'race',
    'sex',
    'capital_gain',
    'capital_loss',
    'hr_per_week',
    'country',
    'income'
]

print(df.shape)

#%%
# Consultando quantos itens temos por país.
counts = df.groupby('country').size()
print(counts.head())

#%%
# Agrupando itens por sexo e também por renda.
ml = df[(df.sex == 'Male')]
ml1 = df[( df. sex == 'Male') & (df.income == '>50K\n')]
fm = df[(df.sex == 'Female')]
fm1 = df[(df.sex == 'Female') & (df.income == '>50K\n')]

# Seção 3.3.1 - Summarizing the Data

#%%
# Pegando os itens com renda alta.
df1 = df[(df.income == '>50K\n')]

print('The rate of people with high income is: ', int(len(df1)/float(len(df))*100), '%.')
print('The rate of men with high income is: ', int(len(ml1)/float(len(ml))*100), '%.')
print('The rate of women with high income is: ', int(len(fm1)/float(len(fm))*100), '%.')

# Seção 3.3.1.1 - Mean

#%%
print('The average age of men is:', ml['age'].mean())
print('The average age of women is: ', fm['age'].mean())
print('The average age of high-income men is: ', ml1['age'].mean())
print('The average age of high-income women is: ', fm1['age'].mean())

# Seção 3.3.1.2 - Sample Variance

# Código que a seção mostra.
# Embora ele fale que está avaliando horas trabalhadas por semana, o código
# avalia apenas a idade.
#%%
ml_mu = ml['age'].mean()
fm_mu = fm['age'].mean()
ml_var = ml['age'].var()
fm_var = fm['age'].var()
ml_std = ml['age'].std()
fm_std = fm['age'].std()

print('Statistics of age for men: mu:', ml_mu, 'var:', ml_var, 'std:', ml_std)
print('Statistics of age for women: mu:', fm_mu, 'var:', fm_var, 'std:', fm_std)

# As estatísticas de horas trabalhadas por semana, que é o que é citado no texto.
# Obs. Os resultados não batem com o que ele descreve.
#%%
ml_mu = ml['hr_per_week'].mean()
fm_mu = fm['hr_per_week'].mean()
ml_var = ml['hr_per_week'].var()
fm_var = fm['hr_per_week'].var()
ml_std = ml['hr_per_week'].std()
fm_std = fm['hr_per_week'].std()

print('Statistics of work hours per week for men: mu:', ml_mu, 'var:', ml_var, 'std:', ml_std)
print('Statistics of work hours per week for women: mu:', fm_mu, 'var:', fm_var, 'std:', fm_std)

# Seção 3.3.1.3 - Sample Median

#%%
ml_median = ml['age'].median()
fm_median = fm['age'].median()

print('Median age per men and women: ', ml_median, fm_median)

#%%
ml_median_age = ml1['age'].median()
fm_median_age = fm1['age'].median()

print('Median age per men and women with high-income: ', ml_median_age, fm_median_age)

# Seção 3.3.2 - Data Distribution

#%%
ml_age = ml['age']
ml_age.hist(histtype='stepfilled', bins=20)

#%%
fm_age = fm['age']
fm_age.hist(histtype='stepfilled', bins=10)

#%%
ml_age.hist(density=True, histtype='step', cumulative=True, linewidth=3.5, bins=20)

#%%
fm_age.hist(density=True, histtype='step', cumulative=True, linewidth=3.5, bins=20)

# Seção 3.3.3 - Outlier Treatment

# %%
df2 = df.drop(df.index[(df.income == '>50K\n') & (df['age'] > df['age'].median() + 35) & (df['age'] > df['age'].median() - 15)])
ml1_age = ml1['age']
fm1_age = fm1['age']
ml2_age = ml1_age.drop(ml1_age.index[(ml1_age > df['age'].median() + 35) & (ml1_age > df['age'].median() - 15)])
fm2_age = fm1_age.drop(fm1_age.index[(fm1_age > df['age'].median() + 35) & (fm1_age > df['age'].median() - 15)])

mu2ml = ml2_age.mean()
std2ml = ml2_age.std()
md2ml = ml2_age.median()
mu2fm = fm2_age.mean()
std2fm = fm2_age.std()
md2fm = fm2_age.median()

print('Men statistics :')
print(' Mean:', mu2ml, ' Std :', std2ml)
print(' Median:', md2ml)
print(' Min:', ml2_age.min(), ' Max :', ml2_age.max())
print('Women statistics:')
print(' Mean:', mu2fm , ' Std :', std2fm)
print(' Median:', md2fm)
print(' Min:', fm2_age.min(), ' Max :', fm2_age.max())

# %%
# plt.figure(figsize=(13.4, 5))
df.age[(df.income == '>50K\n')].plot(alpha=.25, color='blue')
df2.age[(df2.income == '>50K\n')].plot(alpha=.45, color='red')

# %%
print('The mean difference with outliers is: %4.2f.' % (ml_age.mean() - fm_age.mean()))
print('The mean difference without outliers is: %4.2f.' % (ml2_age.mean() - fm2_age.mean()))

# %%
import matplotlib.pyplot as plt
import numpy as np
countx, divisionx=np.histogram(ml2_age, density=True)
county, divisiony=np.histogram(fm2_age, density=True)
val = [(divisionx[i] + divisionx[i +1]) / 2 for i in range(len(divisionx) - 1)]
plt.plot(val, countx - county , 'o-')

# Seção 3.3.4 - Measuring Asymmetry

# %%
def skewness(x):
    res = 0
    m = x.mean()
    s = x.std()
    for i in x:
        res += (i-m) * (i-m) * (i-m)
        res /= (len(x) * s * s * s)
    return res

print('Skewness of the male population = ', skewness(ml2_age))
print('Skewness of the female population is = ', skewness(fm2_age))

# %%
def pearson(x):
    return 3*(x. mean () - x. median ())*x.std ()

print('Pearson’s coefficient of the male population= ', pearson(ml2_age))
print('Pearson’s coefficient of the female population = ', pearson(fm2_age))

# %%
